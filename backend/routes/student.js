const { request, response } = require('express')
const db = require('../db')
const utils = require('../utils')
const express = require('express')
const router = express.Router()

router.post('/addStudent', (request, response) => {
    const { s_name, course, passing_year, prn_no } = request.body
    
    const query = 'insert into student ( s_name, course, passing_year, prn_no ) values(?,?,?,?)'
    db.pool.execute(query, [s_name, course, passing_year, prn_no], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/displayStudent', (request, response) => {
    const query = 'Select student_id, s_name, course, passing_year, prn_no from student'
    db.pool.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('/updateStudent/:student_id', (request, response) => {
    const { student_id } = request.params;
    const { s_name, course, passing_year, prn_no } = request.body
    
    const query = 'update student set s_name = ?, course = ?, passing_year = ?, prn_no = ? where student_id = ?';

    db.pool.execute(query, [s_name, course, passing_year, prn_no, student_id], (error, result) => {
        response.send(utils.createResult(error, result));
    })
})

router.delete("/deleteStudent/:student_id", (request, response) => {
    const { student_id } = request.params;
    const query =
      "delete from student where student_id = ?";
  
    db.pool.execute(query, [student_id], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  });

module.exports = router
